import './ProductCard.css';

export const ProductCard = ({ id, title, price, discount, imageUrl }) => {
    const newPrice = Math.abs(parseInt(price) - (parseInt(price) * parseFloat(discount)));
    return (
        <li className="products-list__item product-card">
            <div className="product-card__top">
                <img src={imageUrl} alt={title} className="product-card__img"/>
            </div>
            <div className="product-card__body">
                <p className="price">
                    { newPrice ? (
                        <>
                            <span className="price__new">{newPrice} ₽</span>
                            <span className="price__old">{price} ₽</span>
                        </>
                        ): 
                    <span><b>{price} ₽</b></span> 
                    }
                </p>
                <h3 className="product-card__title">{title}</h3>
            </div>
        </li>
    )
}