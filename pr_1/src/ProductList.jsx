import { ProductCard } from "./ProductCard";
import './ProductList.css';

export const ProductList = ({ products }) => {
    return (
        <ul className="products-list">
            {products.map((item)=> (
                <ProductCard 
                    key={item.id}
                    title={item.title}
                    price={item.price}
                    discount={item.discount}
                    imageUrl={item.imageUrl}
                />
            ))}
            
        </ul>
    )
};