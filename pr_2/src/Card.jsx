import './Card.css';
import { useRef, useEffect } from 'react';
export const Card = ({
  id,
  title,
  onTitleChange,
  done,
  onToggle,
  onDelete,
}) => {
  const inputRef = useRef();

  useEffect(() => {
      inputRef.current.focus();
  }, []);

  const onKeyDown = e => {
    if (e.key === 'Enter') {
      e.preventDefault(); 
      handleCheckboxChange();
      inputRef.current.blur();
    }
  };
  
  const handleTitleChange = (event) => {
    onTitleChange(id, event.target.value);
  };

  const handleCheckboxChange = () => {
    onToggle(id);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    onToggle(id);
  };

  const handleTitleBlur = () => {
    if (title === '') {
      onDelete(id);
    }
  };
  
  return (
    <form className="card" onSubmit={handleSubmit}>
      <input
        className="card__done"
        type="checkbox"
        checked={done}
        onChange={handleCheckboxChange}
        tabIndex={-1}
      />

      <input
        onKeyDown={onKeyDown}
        ref={inputRef}
        className="card__title"
        type="text"
        value={title}
        onChange={handleTitleChange}
        onBlur={handleTitleBlur}
      />
    </form>
  );
};
