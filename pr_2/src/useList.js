import { useRef, useState } from "react"; 
export function useList() {
  const [list, setItem] = useState([
    {
      id: 1,
      title: 'Хлеб',
      done: false,
    },
    {
      id: 2,
      title: 'Молоко',
      done: true,
    },
    {
      id: 3,
      title: 'Сметана',
      done: false,
    }
  ]);

  /** Создать новый элемент. */
  const createItem = () => {
    
    const newItem = {
      id: Date.now(),
      title: '',
      done: false,
    };

    setItem((prevList) => [...prevList, newItem]);
  };

  /**
   * Установить заголовок элемента.
   *
   * @param id - ID элемента.
   * @param title - Заголовок элемента.
   */
  const setItemTitle = (id, title) => {
    setItem((prevList) =>
      prevList.map((item) =>
        item.id === id ? { ...item, title } : item
      )
    );
  };

  /**
   * Переключить выполненность элемента.
   *
   * @param id - ID элемента.
   */
  const toggleItem = (id) => {
    setItem((prevList) =>
      prevList.map((item) =>
        item.id === id ? { ...item, done: !item.done } : item
      )
    );
  };

  /**
   * Удалить элемент.
   *
   * @param id - ID элемента.
   */
  const deleteItem = (id) => {
    const updatedList = list.filter(item => item.id !== id);
  
    setItem(updatedList);
  };


  return {
    list,
    createItem,
    setItemTitle,
    toggleItem,
    deleteItem,
  };
}
